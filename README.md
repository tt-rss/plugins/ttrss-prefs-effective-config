# Effective Config Tiny Tiny RSS Plugin

## Overview
This plugin adds an "Effective Config" section to Preferences → System, with the goal of making it
a bit easier to see what the live configuration is and how each value was determined.

## Installation
### Plugin Installer
1. Navigate to Preferences → Plugins in tt-rss
2. Click "Install plugin"
3. Locate **ttrss-prefs-effective-config** and click "Install"
4. Enable the plugin @ Preferences → Plugins

### Git
1. Clone the repo to **prefs_effective_config** in your tt-rss **plugins.local** directory:

   `git clone https://dev.tt-rss.org/tt-rss/ttrss-prefs-effective-config.git prefs_effective_config`

2. Enable the plugin @ Preferences → Plugins
